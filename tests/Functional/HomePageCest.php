<?php

class HomePageCest
{
    public function welcomeMessage(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Welcome to the Symfony Demo application');
    }
}
